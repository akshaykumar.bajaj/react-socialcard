import React, { Component } from 'react'
import logo from '../logo.svg';
import '../App.css';

export class SocialCard extends Component {
  constructor(props) {
    super(props);

    this.state = {
      ...this.props.state,
      showComentBox : false,
    };
  }
  incrementLikeCount = () => {
    this.setState({
      likeCouter: this.state.likeCouter + 1,
    })
  }

  incrementRepostCount = () => {
    this.setState({
      repostCounter: this.state.repostCounter + 1,
    })
  }

  showComentBox = () => {
    this.setState({
      showComentBox: true,
    });
  }
  closeComment = () => {
    this.setState({
      showComentBox: false,
    });
  }
  // commentContainerStyle =() => {
  //   return {
  //     // display: this.state.showComentBox? 'flex': 'none'
  //   }
  // }
  render() {
    return (
      <div className="App">
        <div className="social-card">
          <div className="wrapper">
            <img src={logo} className="App-logo" alt="logo" />
            <header className="App-header">
              <div className="titleLine1">
                <p id="name">{this.state.name}</p>
                <p id="username"> @{this.state.username} .{this.state.date} </p>
              </div>
              <div className="titleLine2">
                <p>{this.state.title}</p>
              </div>
              <div className="titleLine3">
                <p>{this.state.authorUserId}</p>
              </div>
            </header>
          </div>
          <main>
            <div className="thumbnail">
              <img src={logo} className="App-logo Thumnail-logo" alt="logo" />
              <h1>{this.state.title}</h1>
              <div className="byLine">
                {this.state.authorName}
              </div>
            </div>
            <p>{this.state.title}</p>
          </main>
          <footer>
            <div className="buttonWrapper" >
              <button className="likeBtn"
                onClick={this.incrementLikeCount}
              />
              <p>{this.state.likeCouter}</p>
            </div>

            <div className="buttonWrapper">
              <button className="repostBtn"
                onClick={this.incrementRepostCount}
              />
              <p>{this.state.repostCounter}</p>
            </div>

            <div className="buttonWrapper">
              <button className="commentBtn"
              onClick={this.showComentBox}
              />
              <p>{this.state.comments.length}</p>
            </div>
          </footer>
        </div>
        <div className="commentContainer" style={{display: this.state.showComentBox? 'flex': 'none'}}>
          <div className="commentCard">
            <header>Comments
            <button className="closeComentButton" onClick={this.closeComment}>X</button>
            </header>
            {this.state.comments.map(comment =>
              console.log(comment)
            )}
          </div>
        </div>
      </div>
    );
  }
}

export default SocialCard
