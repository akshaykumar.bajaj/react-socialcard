import React, { Component } from 'react'
import SocialCard from './SocialCard';


export class Feeds extends Component {
    constructor(props){
        super(props);
        this.state = {
            name: 'The Practical Dev',
            username: 'ThePracticalDev',
            date: 'Sep 10',
            title: 'Learning React? Start Small',
            authorUserId: '@dceddia',
            authorName: 'Dave Ceddia',
            authorImageUrl: '',
            likeCouter: 0,
            comments: [{
                id: 1,
                name: "Akshay",
                username: "@apb",
                comment:"Good Post",
            }, {
                id: 2,
                name: "Deepanshu",
                username: "@deep",
                comment:"Very Well Explained",
            }],
            repostCounter: 0,
          }
    }
    render() {
        return (
            <div>
                <SocialCard state={this.state} />
            </div>
        )
    }
}

export default Feeds
